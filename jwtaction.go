package robotjwt

import (
	"fmt"
	"time"
	"github.com/dgrijalva/jwt-go"
	"bitbucket.org/cooli777/roboterror"
)

const (
     DefaultUserID = 0
     DefaultRoleGuest = "guest"
     DefaultUserNickName = "guest"
     DefaultUserStatus = "not_confirmed"
     DefaultUserEmail = ""
)

// NewTokenWithCustomClaims create token with custom claims
func NewTokenWithCustomClaims(user UserJwt, jwtSecretKey string) string {
	mySigningKey := []byte(jwtSecretKey)
	expireToken := time.Now().Add(time.Hour * 24).Unix()
	issuer := "https://swappi.dev"

	claims := createCustomClaims(user, expireToken, issuer)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, _ := token.SignedString(mySigningKey)
	return fmt.Sprintf("%v", ss)
}

// ParseWithCustomClaimsType parse and vallidate token, and get custom claims
func ParseWithCustomClaimsType(tokenString string, jwtSecretKey string) (*RobotClaims, *roboterror.ErrorApp) {

	//parse string token
	token, err := jwt.ParseWithClaims(tokenString, &RobotClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtSecretKey), nil
	})

	if errorToken := errorCheckingAfterParseToken(token, err); errorToken != nil {
		return nil, errorToken
	}

	claims, ok := token.Claims.(*RobotClaims)

	if ok {
		//fmt.Printf("%v %v", claims.NickName, claims.StandardClaims.ExpiresAt)
	} else {
		fmt.Println(ok)
	}

	return claims, nil
}

// errorCheckingAfterParseToken checking error after parse
func errorCheckingAfterParseToken(token *jwt.Token, err error) *roboterror.ErrorApp {

	var result *roboterror.ErrorApp

	if token != nil && token.Valid {
		return nil
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			result = Error.ThatIsNotToken()
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			result = Error.TokenIsExpired()
		} else {
			result = Error.TokenCouldnNotHandle(err)
		}
	} else {
		result = Error.TokenCouldnNotHandle(err)
	}

	return result
}

// createCustomClaims create and return custom claims
func createCustomClaims(user UserJwt, expireToken int64, issuer string) RobotClaims {
	return RobotClaims{
		user.GetId(),
		user.GetRole(),
		user.GetNickName(),
		user.GetStatus(),
		user.GetEmail(),
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    issuer,
		},
	}
}

func CreateEmptyClaims() *RobotClaims {
	return &RobotClaims{
		DefaultUserID,
		DefaultRoleGuest,
		DefaultUserNickName,
		DefaultUserStatus,
		DefaultUserEmail,
		jwt.StandardClaims{
			ExpiresAt: 0,
			Issuer:    "https://swappi.dev",
		},
	}
}

func JwtCheckAndCreateClaims(tokenString string, jwtSecretKey string) (*RobotClaims, *roboterror.ErrorApp) {
	var (
		claims *RobotClaims
		err    *roboterror.ErrorApp
	)

	if tokenString != "" {
		claims, err = ParseWithCustomClaimsType(tokenString, jwtSecretKey)
	} else {
		claims = CreateEmptyClaims()
	}

	return claims, err
}
