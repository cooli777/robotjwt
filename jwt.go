package robotjwt

import "github.com/dgrijalva/jwt-go"

type UserJwt interface {
	GetId() uint
	GetRole() string
	GetNickName() string
	GetStatus() string
	GetEmail() string
}

//todo создать интерфейс Claims
type RobotClaims struct {
	UserId   uint `json:"id"`
	Role     string `json:"role"`
	NickName string `json:"nick_name"`
	Status   string `json:"status"`
	Email    string `json:"email"`
	jwt.StandardClaims
}

func (rc RobotClaims) GetId() uint {
	return rc.UserId
}

func (rc RobotClaims) GetRole() string {
	return rc.Role
}

func (rc RobotClaims) GetNickName() string {
	return rc.NickName
}

func (rc RobotClaims) GetStatus() string {
	return rc.Status
}

func (rc RobotClaims) GetEmail() string {
	return rc.Email
}


