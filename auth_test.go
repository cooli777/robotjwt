package robotjwt

import (
	"testing"
	"strings"
	"reflect"
	"api.swappi/models"
	"api.swappi/customError"
	"github.com/dgrijalva/jwt-go"
	"api.swappi/rbac"
)

func TestNewTokenWithCustomClaims(t *testing.T) {
	const lenBlocks = 3

	var userAppWant = models.UserApp{
		Id : 1,
		Role : rbac.Roles.RoleCustomer(),
		Email : "test@email.email",
		NickName : "testNick",
		Login : "testLogin",
		Status : models.UserAppStatusActive,
		Deleted : false,
		PasswordHash : "123",
		CreatedAt : 100,
		UpdatedAt : 100,
	}

	var token string = NewTokenWithCustomClaims(userAppWant)

	blocks := strings.Split(token, ".")

	if len(blocks) != lenBlocks {
		t.Errorf("%v; want %v", len(blocks), lenBlocks)
	}

	myCustomClaimsGot, err := ParseWithCustomClaimsType(token)

	if err != nil {
		t.Errorf("Error: want %v", err)
	}

	var myCustomClaimsWant = &RobotClaims{
		userAppWant.Id,
		userAppWant.Role,
		userAppWant.NickName,
		userAppWant.Status,
		userAppWant.Email,
		jwt.StandardClaims{
			ExpiresAt: myCustomClaimsGot.ExpiresAt,
			Issuer:    myCustomClaimsGot.Issuer,
		},
	}

	if !reflect.DeepEqual(myCustomClaimsWant, myCustomClaimsGot) {
		t.Errorf("Want %v;\n Got %v", myCustomClaimsWant, myCustomClaimsGot)
	}

	//todo проверить возвращаемые ошибки

	_, errGot := ParseWithCustomClaimsType("asdsadasd")

	errorWant := customError.ErrosAll.Jwt.ThatIsNotToken()
	if !reflect.DeepEqual(errorWant, errGot) {
		t.Errorf("Want %v;\n Got %v", errorWant, errGot)
	}
}