package robotjwt

import (
	"bitbucket.org/cooli777/roboterror"
	"fmt"
)

type ErrorsJwt struct{}

var Error ErrorsJwt = ErrorsJwt{}

var (
	thatIsNotToken    *roboterror.ErrorApp = roboterror.Create("That's not even a token", "That's not even a token", 10001, true, roboterror.Low_lewel)
	jwtTokenIsExpired *roboterror.ErrorApp = roboterror.Create("Token is expired ", "Token is expired", 10002, true, roboterror.Low_lewel)
)

func (ErrorsJwt) JWTNotValid(err error) *roboterror.ErrorApp {
	return roboterror.CreateWithError(err, "JWT not valid json", 10000, true, roboterror.Low_lewel)
}

func (ErrorsJwt) ThatIsNotToken() *roboterror.ErrorApp {
	return thatIsNotToken
}

func (ErrorsJwt) TokenIsExpired() *roboterror.ErrorApp {
	return jwtTokenIsExpired
}

func (ErrorsJwt) TokenCouldnNotHandle(err error) *roboterror.ErrorApp {
	return roboterror.CreateWithError(err, fmt.Sprintf("Couldn't handle this token: %s", err), 10003, true, roboterror.Low_lewel)
}
